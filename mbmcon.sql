select * from tab;

--Case1: roll,First_name,Mid_name,Last_name,Conctact_no,Email,DOB
create table student(
roll number(3),
first_name varchar2(30),
mid_name varchar2(30),
last_name varchar2(30),
contact_no number(30),
email varchar2(40),
dob date
);

select * from tab;
desc student;

insert into student values (1,'Bibek','','Khatiwada','12345','bibekkhatiwada16@gmail.com',sysdate);
desc student;
select * from student;



 
/*Case2: roll,First_name,Mid_name,Last_name,Conctact_no,Email,DOB (Defining new UDT) */

Create or replace type personname as object (
first_name varchar2(30),
mid_name varchar2(30),
last_name varchar2(30));

create table student2(
roll number(3),
student_name personname,
contact_no number(10),
email varchar2(40),
dob date);

desc student2;
select * from student2;

insert into student2 values (1,personname('Bibek','','Khatiwada'),12345,'bibekkhatiwada16@gmail.com',sysdate); 
select * from student2;


--Case 3: Encapsulate(First,Mid,Last Name, DOB)  return age /*roll, First_Name, Mid_Name, Last_Name, Contact_no, email, DOB*/

--roll, (First_Name, Mid_Name, Last_Name, DOB), contact_no, email
--getAge()  ---Return

create or replace type personnameage as object(
first_name varchar2(30),
mid_name varchar2(30),
last_name varchar2(30)
dob date,
Member function getAge return number);

--PLSQL
/
create or replace type body personnameage as
Member function getAge return number as
begin
return trunc(Months_between(sysdate,dob)/12);
end getAge;
end;
/


Create table student3(
roll number(3),
student_name personnameage,
contact_no number(10),
email varchar2(40)
);
select * from tab;

desc student3;

Insert into student3 values(1,personnameage('Ram','Kumar','Singh','01/JAN/80'),123456,'ram@testmail.com')
select * from student3;


select s.ROLL, s.student_name.first_name,s.student_name.mid_name,s.student_name.last_name,
s.student_name.DOB,s.contact_no,s.email from student3 s;

select s.ROLL, s.student_name.first_name,s.student_name.mid_name,s.student_name.last_name,s.student_name.DOB,
to_char(s.student_name.DOB,'DD/MON/YYYY'),s.contact_no,s.email, s.student_name.getAge() as age from student3 s;


Insert into student3 values(1,personnameage('Shyam','Kumar','Thgunna',to_date('01/JAN/1880','DD/MON/YYYY')),123456,'ram@testmail.com')


